# measures-scripts

Scripts for finalizing miscellaneous MEaSUREs datasets.

Note: These are the succession of work done by Terry Haran.  [See confluence page on Terry's work](https://nsidc.org/confluence/display/DAACPROD/Terry+Haran-Information) and the code preserved in these repositories:

* https://bitbucket.org/nsidc/tmh_measures_ant_vel
* https://bitbucket.org/nsidc/tmh_measures_ase_vel
* https://bitbucket.org/nsidc/tmh_measures_gimp
* https://bitbucket.org/nsidc/tmh_measures_netcdf


## Installation

Using [conda](https://docs.conda.io/en/latest/):

1. Install dependencies: `conda env create -f environment.yml`
1. Activate environment: `conda activate measures`
```

## NSIDC0725

`python nsidc0275.py --help`

Typical procedure:

1. Covert geodat to tif (and rename shapefiles): `python nsidc0725.py convert_tif -i incoming/nsidc0725 -o staged/nsidc0725`
1. Generate magnitude tifs: `python nsidc0725.py compute_magnitudes -i staged/nsidc0725 -o staged/nsidc0725`

## NSIDC0478

`python nsidc0478.py --help`

Typical procedure:

1. Covert geodat to tif: `python nsidc0478.py convert_tif -i incoming/nsidc0478 -o staged/nsidc0478`
1. Generate magnitude tifs: `python nsidc0478.py compute_magnitudes -i staged/nsidc0478 -o staged/nsidc0478`
1. Generate browse images: `python nsidc0478.py convert_browse -i staged/nsidc0478 -o staged/nsidc0478`

## NSIDC0481

`python nsidc0481.py --help`

Typical procedure:

1. Covert geodat to tif: `python nsidc0481.py convert_tif -i incoming/nsidc0481 -o staged/nsidc0481`

## NSIDC0731

`python nsidc0731.py --help`

Typical procedure:

1. Rename files: `python nsidc0731.py convert_tif -i incoming/nsidc0731 -o staged/nsidc0731`
