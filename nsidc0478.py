import click

import run.nsidc0478.browse as browse
import run.nsidc0478.data as data
import run.nsidc0478.magnitude as magnitude


@click.group()
def cli():
    '''
    Commands for processing raw NSIDC0478 data
    '''
    pass


@cli.command()
@click.option('-i', '--input-directory', help='Input directory')
@click.option('-o', '--output-directory', help='Output directory')
def convert_tif(**kwargs):
    data.convert(**kwargs)


@cli.command()
@click.option('-i', '--input-directory', help='Input directory')
@click.option('-o', '--output-directory', help='Output directory')
def compute_magnitudes(**kwargs):
    magnitude.generate(**kwargs)


@cli.command()
@click.option('-i', '--input-directory', help='Input directory')
@click.option('-o', '--output-directory', help='Output directory')
def convert_browse(**kwargs):
    browse.convert(**kwargs)


@cli.command()
@click.option('-i', '--input-directory', help='Input directory')
@click.option('-o', '--output-directory', help='Output directory')
def v2_to_v21(**kwargs):
    data.v2_to_v21(**kwargs)


if __name__ == '__main__':
    cli()
