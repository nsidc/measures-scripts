import click

from run.nsidc0481.convert import convert_directory


@click.group()
def cli():
    '''
    Commands for processing raw NSIDC0481 data
    '''
    pass


@cli.command()
@click.option('-i', '--input-directory', help='Input directory')
@click.option('-o', '--output-directory', help='Output directory')
def convert_tif(**kwargs):
    convert_directory(**kwargs)


if __name__ == '__main__':
    cli()
