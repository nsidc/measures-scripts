import re
import pickle
import hashlib


class FilenamePatternError(Exception):
    pass


class FilenamePattern():
    '''
    A combination of filename string template with named regex groups.

    Supports matching with the 'match' method and interpolation with the 'format' method.

    >>> p = FilenamePattern(
            '{a}_{b}.bin',
            {'a': '[a-z]+',
             'b': '\d+'}
        )

    >>> p.match('g_1.bin'))
    {'a': 'g', 'b': '1'}

    >>> p.format(a='h', b=100)
    'h_100.bin'
    '''

    @staticmethod
    def _escape_template(template):
        return re.escape(template).replace('\\{', '{').replace('\\}', '}')

    def __init__(self, template, pieces):
        self.pieces = pieces
        self._template = template

        init_error = FilenamePatternError('Pieces \'{}\' inconsistant with template \'{}\''
                                          ''.format(tuple(self.pieces.keys()), template))

        groups = {key: r'(?P<{}>{})'.format(key, regex) for key, regex in pieces.items()}
        try:
            self._re = re.compile(self._escape_template(self._template).format(**groups))
        except KeyError:
            # Must be missing pieces
            raise init_error

        # Check no extra pieces
        if not all('{' + key + '}' in template for key in pieces):
            raise init_error

    def match(self, s):
        m = self._re.match(s)
        if m is None:
            return None

        return {k: v for k, v in m.groupdict().items() if v is not None}

    def _check_keys(self, d):
        extra_pieces = set(d) - set(self.pieces)
        if extra_pieces:
            raise FilenamePatternError('Keys provided inconsistant with pattern: '
                                       '{} != {}'.format(tuple(d.keys()),
                                                         tuple(self.pieces.keys())))

    def format(self, **kwargs):
        self._check_keys(kwargs)

        return self._template.format(**kwargs)

    def format_map(self, d):
        self._check_keys(d)

        return self._template.format_map(d)
