from pathlib import Path
import numpy as np
from numpy.linalg import norm
import rasterio

INPUT_NODATA = -2000000000
OUTPUT_NODATA = -0.1


def generate_magnitudes(input_directory, output_directory):
    for vx_path in Path(input_directory).glob("*_vx_*tif"):
        vy_path = Path(input_directory) / vx_path.name.replace("_vx_", "_vy_")
        compute_magnitude(vx_path, vy_path, output_directory)


def compute_magnitude(vx_path, vy_path, output_directory):
    output_path = Path(output_directory) / vx_path.name.replace("_vx_", "_vv_")

    print(f"Converting {vx_path} and {vy_path} to {output_path}")

    with rasterio.open(str(vx_path), "r", driver="GTiff") as vx:
        profile = vx.profile
        vx_data = vx.read(1).astype(np.float64)

    with rasterio.open(str(vy_path), "r", driver="GTiff") as vy:
        vy_data = vy.read(1).astype(np.float64)

    mask = np.logical_and(vx_data < INPUT_NODATA + 1, vy_data < INPUT_NODATA + 1)
    vv_data = norm(np.dstack([vx_data, vy_data]), axis=2).astype(np.float32)
    vv_data[mask] = OUTPUT_NODATA

    with rasterio.open(str(output_path), "w", **profile) as vv:
        vv.write(vv_data, 1)
        # vv.nodata = OUTPUT_NODATA
