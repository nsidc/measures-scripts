from pathlib import Path
import sys
import shutil

import rasterio.crs as crs

from ..io import geodat2geotif
from ..util import FilenamePattern


NODATA = int(-2e9)
DTYPE = ">f4"
CRS = crs.CRS({"init": "epsg:3413"})
VERSION = "v01"


GEODAT_FILENAME_PATTERN = FilenamePattern(
    template=r"{root}{year}{temporal}-{resolution}.{version}.{ext}",
    pieces={
        "root": "mosaic",
        "year": "\d{4}",
        "temporal": "annual",
        "resolution": "\d\.\dkm",
        "version": "V\d+",
        "ext": "[a-z]{2}",
    },
)

LANDSAT_SHAPE_FILENAME_PATTERN = FilenamePattern(
    template=r"{root}{year}{temporal}.{version}.{ext}",
    pieces={
        "root": "mosaic",
        "year": "\d{4}",
        "temporal": "annual",
        "version": "V\d+",
        "ext": "shp",
    },
)

SAR_SHAPE_FILENAME_PATTERN = FilenamePattern(
    template=r"{root}{year}.{ext}",
    pieces={"root": "LandsatSource", "year": "\d{4}", "ext": "shp"},
)

TIF_OUTPUT_FILENAME_TEMPLATE = (
    r"greenland_vel_mosaic{resolution}_{year}_{type}_{version}.{ext}"
)

SHAPEFILE_OUTPUT_FILENAME_TEMPLATE = (
    r"greenland_vel_mosaic_{year}_{type}_{version}.{ext}"
)


def convert_directory(input_directory, output_directory):
    for geodat_path in Path(input_directory).glob("**/*.geodat"):
        input_path = geodat_path.with_suffix("")
        parts = GEODAT_FILENAME_PATTERN.match(input_path.name)
        output_path = str(Path(output_directory) / TIF_OUTPUT_FILENAME_TEMPLATE).format(
            resolution=int(float(parts["resolution"].replace("km", "")) * 1000),
            version=VERSION,
            year=parts["year"],
            type=parts["ext"],
            ext="tif",
        )
        geodat2geotif(input_path, output_path, dtype=DTYPE, crs=CRS)


def convert_shapefiles(input_directory, output_directory):
    for shapefile_path in Path(input_directory).glob("**/*.shp"):
        parts = LANDSAT_SHAPE_FILENAME_PATTERN.match(shapefile_path.name)
        if parts:
            t = "L8"
        else:
            parts = SAR_SHAPE_FILENAME_PATTERN.match(shapefile_path.name)
            if parts:
                t = "SAR"
            else:
                raise Exception("Unexpected shapefile: {}".format(shapefile_path))

        for ext in ("dbf", "prj", "shx", "shp"):
            output_filepath = Path(
                output_directory
            ) / SHAPEFILE_OUTPUT_FILENAME_TEMPLATE.format(
                year=parts["year"], type=t, version=VERSION, ext=ext
            )
            shutil.copy(str(shapefile_path), str(output_filepath))
