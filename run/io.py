from pathlib import Path

import numpy as np
import rasterio
from rasterio.transform import from_origin


def read_geodat(filepath):
    grid_data = np.loadtxt(str(filepath), comments=('#', ';', '&'))
    cols, rows = grid_data[0].astype(int)
    res_x, res_y = grid_data[1]
    ll_x, ll_y = grid_data[2] * 1000  # To convert from km to m

    return {
        'rows': rows,
        'cols': cols,
        'res_x': res_x,
        'res_y': res_y,
        'll_x': ll_x,
        'll_y': ll_y,
    }


def write_geotiff(
    filepath, data, dtype, rows, cols, res_x, res_y, ll_x, ll_y, crs, nodata=None
):
    transform = from_origin(
        ll_x - res_x / 2, ll_y + rows * res_y - res_y / 2, res_x, res_y
    )

    with rasterio.open(
        str(filepath),
        'w',
        driver='GTiff',
        height=rows,
        width=cols,
        count=1,
        dtype=str(data.dtype),
        crs=crs,
        transform=transform,
    ) as geotif:
        geotif.write(data, 1)
        if nodata is not None:
            geotif.nodata = nodata


def geodat2geotif(input_path, output_path, dtype, crs, nodata=None):
    print(f'Converting {input_path} to {output_path}')

    geodat_path = Path(str(input_path) + '.geodat')
    params = read_geodat(geodat_path)

    # Read binary data into np.float32 array
    data = np.flipud(
        np.fromfile(str(input_path), dtype=dtype).reshape(
            (params['rows'], params['cols'])
        )
    ).astype(np.float32)

    write_geotiff(
        filepath=output_path, data=data, dtype=dtype, crs=crs, nodata=nodata, **params
    )
