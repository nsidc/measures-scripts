from pathlib import Path
import shutil

import rasterio
import rasterio.crs as crs

from ..io import geodat2geotif
from ..util import FilenamePattern


NODATA = int(-2e9)
NODATA_VV = -1
DTYPE = '>f4'
CRS = crs.CRS({'init': 'epsg:3413'})
VERSION = 'v02.1'

FILENAME_PATTERN = FilenamePattern(
    template=r'{root}{resolution}_{start_year}_{end_year}_{version}.{ext}',
    pieces={
        'root': 'greenland_vel_mosaic',
        'resolution': '\d+',
        'start_year': '\d{4}',
        'end_year': '\d{4}',
        'version': 'v\d+',
        'ext': '[a-z]{2}'
    }
)
OUTPUT_FILENAME_PATTERN = FilenamePattern(
    template=r'{root}{resolution}_{start_year}_{end_year}_{type}_{version}.{ext}',
    pieces={
        'root': 'greenland_vel_mosaic',
        'resolution': '\d+',
        'start_year': '\d{4}',
        'end_year': '\d{4}',
        'version': 'v\d+',
        'type': '[a-z]+',
        'ext': '[a-z]+'
    }
)


def get_output_filepath(input_filepath, output_directory):
    pieces = FILENAME_PATTERN.match(str(input_filepath.name))
    pieces['version'] = VERSION
    output_filename = ('{root}{resolution}_{start_year}_{end_year}'
                       '_{ext}_{version}.tif').format(**pieces)
    output_filepath = Path(output_directory) / output_filename

    return output_filepath


def convert(input_directory, output_directory):
    for geodat_filepath in Path(input_directory).glob('**/*.geodat'):
        input_filepath = geodat_filepath.with_suffix('')
        output_filepath = get_output_filepath(input_filepath, output_directory)

        geodat2geotif(input_filepath, output_filepath, dtype=DTYPE, crs=CRS, nodata=NODATA)


def v2_to_v21(input_directory, output_directory):
    # replace float nodata value in vel
    # add NODATA attribute for vv files -1, others -2e9
    # rename vel to vv
    # rename _v2. to _v02.1.
    for input_filepath in Path(input_directory).glob('**/*'):
        if input_filepath.is_dir():
            continue
        subdir = input_filepath.parent.relative_to(input_directory)
        if input_filepath.name.endswith('tif'):
            pieces = OUTPUT_FILENAME_PATTERN.match(str(input_filepath.name))
            pieces['version'] = 'v02.1'
            nodata = NODATA_VV if pieces['type'] == 'vel' else NODATA
            with rasterio.open(str(input_filepath), 'r') as f:
                profile = f.profile
                data = f.read()

            if pieces['type'] == 'vel':
                pieces['type'] = 'vv'
                data[data < 0] = nodata

            output_filename = OUTPUT_FILENAME_PATTERN.format(**pieces)
            output_filepath = (Path(output_directory) /
                               subdir /
                               output_filename)
            output_filepath.parent.mkdir(parents=True, exist_ok=True)
            with rasterio.open(str(output_filepath), 'w', **profile) as f:
                f.write(data)
                f.nodata = nodata
        else:
            output_filename = input_filepath.name.replace('_v2.', '_v02.1.')
            output_filepath = (Path(output_directory) /
                               subdir /
                               output_filename)
            output_filepath.parent.mkdir(parents=True, exist_ok=True)
            try:
                shutil.copy(input_filepath, output_filepath)
            except FileNotFoundError:
                print(f'could not copy {input_filepath}')
