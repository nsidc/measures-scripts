from pathlib import Path

from PIL import Image
import numpy as np
import rasterio

from .data import FILENAME_PATTERN, VERSION


def get_output_filepath(input_filepath, output_directory):
    pieces = FILENAME_PATTERN.match(str(input_filepath.name))
    pieces['version'] = VERSION
    output_filename = ('{root}{resolution}_{start_year}_{end_year}'
                       '_browse_{version}.jpg').format(**pieces)

    return Path(output_directory) / output_filename


def convert(input_directory, output_directory):
    for input_filepath in Path(input_directory).glob('**/*.tif'):
        output_filepath = get_output_filepath(input_filepath, output_directory)
        print(f'Converting {input_filepath} to {output_filepath}')

        with rasterio.open(str(input_filepath), 'r') as geotif:
            rgb = np.moveaxis(geotif.read(), 0, -1)

        image = Image.fromarray(rgb)
        image.save(output_filepath)
