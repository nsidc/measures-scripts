from pathlib import Path

import numpy as np
from numpy.linalg import norm
import rasterio

INPUT_NODATA = -2000000000
OUTPUT_NODATA = -1


def generate(input_directory, output_directory):
    for vx_filepath in Path(input_directory).glob('*_vx_*tif'):
        vy_filepath = Path(input_directory) / vx_filepath.name.replace('_vx_', '_vy_')
        compute_magnitude(vx_filepath, vy_filepath, output_directory)


def compute_magnitude(vx_filepath, vy_filepath, output_directory):
    output_filepath = Path(output_directory) / vx_filepath.name.replace('_vx_', '_vv_')

    print(f'Converting {vx_filepath} and {vy_filepath} to {output_filepath}')

    with rasterio.open(str(vx_filepath), 'r', driver='GTiff') as vx:
        profile = vx.profile
        vx_data = vx.read(1).astype(np.float64)

    with rasterio.open(str(vy_filepath), 'r', driver='GTiff') as vy:
        vy_data = vy.read(1).astype(np.float64)

    mask = np.logical_and(vx_data < INPUT_NODATA + 1,
                          vy_data < INPUT_NODATA + 1)
    vv_data = norm(np.dstack([vx_data, vy_data]), axis=2).astype(np.float32)
    vv_data[mask] = OUTPUT_NODATA

    with rasterio.open(str(output_filepath), 'w', **profile) as vv:
        vv.write(vv_data, 1)
        vv.nodata = OUTPUT_NODATA
