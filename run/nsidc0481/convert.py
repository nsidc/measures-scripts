import shutil
from pathlib import Path
import os

import rasterio.crs as crs

from ..io import geodat2geotif
from ..util import FilenamePattern

NODATA = -2e-9
DTYPE = ">f4"
CRS = crs.CRS({"init": "epsg:3413"})

FILENAME_PATTERN = FilenamePattern(
    template=r"{root}{direction}coast-{grid}/TSX_{start_date}_{end_date}_{numbers}/mosaicOffsets.{ext}",
    pieces={
        "root": ".*",
        "direction": "[A-Z]",
        "grid": "[\d\.]+[A-Z]?",
        "start_date": "[a-zA-Z]+-\d{2}-\d{4}",
        "end_date": "[a-zA-Z]+-\d{2}-\d{4}",
        "numbers": "\d+-\d+-\d+",
        "ext": ".+",
    },
)


def reformat_date(s):
    m, d, y = s.split("-")

    return f"{d}{m}{y[-2:]}"


def get_output_filepath(input_path, output_directory):
    pieces = FILENAME_PATTERN.match(str(input_path))
    grid_subdir = "{direction}coast-{grid}".format(**pieces)
    next_subdir = "TSX_{start_date}_{end_date}_{numbers}".format(**pieces)
    other_start_date = reformat_date(pieces["start_date"])
    other_end_date = reformat_date(pieces["end_date"])
    filename = (
        (
            "TSX_{direction}{grid}"
            "_{other_start_date}"
            "_{other_end_date}"
            "_{numbers}".format(
                other_start_date=other_start_date,
                other_end_date=other_end_date,
                **pieces,
            )
        )
        + input_path.suffix.replace(".", "_")
        + "_v1.2.tif"
    )
    output_path = output_directory / grid_subdir / next_subdir / filename

    return output_path


def convert_directory(input_directory, output_directory):
    input_directory = Path(input_directory)
    output_directory = Path(output_directory)
    for geodat_path in input_directory.glob("**/*.geodat"):
        input_path = geodat_path.with_suffix("")
        output_path = get_output_filepath(input_path, output_directory)
        output_path.parent.mkdir(parents=True, exist_ok=True)

        if not output_path.exists():
            geodat2geotif(input_path, output_path, dtype=DTYPE, crs=CRS, nodata=NODATA)

        suffix_part = input_path.suffix.replace(".", "_")
        output_stem = output_path.with_suffix("").name.replace(suffix_part, "")

        input_meta_path = input_path.with_suffix(".meta")
        output_meta_path = output_path.parent / (output_stem + ".meta")
        if not output_meta_path.exists():
            shutil.copyfile(input_meta_path, output_meta_path)

        input_jpg_path = next(input_path.parent.glob("*.jpg"))
        output_jpg_path = output_path.parent / (output_stem + ".jpg")
        if not output_jpg_path.exists():
            shutil.copyfile(input_jpg_path, output_jpg_path)
