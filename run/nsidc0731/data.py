import shutil
from pathlib import Path
import os
import logging

from ..util import FilenamePattern


logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s %(message)s")

COMMON_PIECES = {
    "root": ".*",
    "start_date_a": "\d{4}-\d{2}-\d{2}",
    "end_date_a": "\d{4}-\d{2}-\d{2}",
    "start_date_b": "\d{4}-\d{2}-\d{2}",
    "end_date_b": "\d{4}-\d{2}-\d{2}",
    "ext": "[A-Za-z\.]+$",
}
VERSION = "v01"
PREFIX = "greenland_vel_mosaic"


INPUT_FILEPATH_PATTERN = FilenamePattern(
    template=r"{root}/Vel-{start_date_a}.{end_date_a}/release/{prefix}{start_date_b}.{end_date_b}.{ext}",
    pieces=dict(**{"prefix": "(Vel-|LS8\.|SAR\.)"}, **COMMON_PIECES),
)

OUTPUT_FILEPATH_PATTERN = FilenamePattern(
    template=(
        r"{root}/{prefix_a}_{start_date_a}_{end_date_a}_{version_a}/"
        "{prefix_b}_{start_date_b}_{end_date_b}_{variable}_{version_b}.{ext}"
    ),
    pieces=dict(
        **{
            "prefix_a": PREFIX,
            "prefix_b": PREFIX,
            "version_a": VERSION,
            "version_b": VERSION,
            "variable": "[A-Za-z]*",
        },
        **COMMON_PIECES,
    ),
)


def get_output_filepath(pieces, output_directory):
    pieces = pieces.copy()
    pieces.update(
        {
            "prefix_a": PREFIX,
            "prefix_b": PREFIX,
            "version_a": VERSION,
            "version_b": VERSION,
            "root": str(Path(output_directory)),
        }
    )
    prefix = pieces["prefix"]
    if prefix == "LS8.":
        variable = "L8"
    elif prefix == "SAR.":
        variable = "SAR"
    else:
        ext_split = pieces["ext"].split(".")
        variable = ext_split[0]
        if variable == "logvmap":
            variable = "browse"
        elif variable == "v":
            variable = "vv"
        pieces["ext"] = ".".join(ext_split[1:])

    pieces["variable"] = variable

    pieces.pop("prefix", None)

    output_filepath = Path(OUTPUT_FILEPATH_PATTERN.format(**pieces))

    return output_filepath


def convert_filepaths(input_directory, output_directory, dry_run=False):
    input_directory = Path(input_directory)
    output_directory = Path(output_directory)
    for input_filepath in input_directory.glob("**/*"):
        logging.info(f"Checking file: {input_filepath}")
        pieces = INPUT_FILEPATH_PATTERN.match(str(input_filepath))
        if pieces is not None:
            output_filepath = get_output_filepath(pieces, output_directory)
            logging.info(f"Moving file:\n    {input_filepath}\n    {output_filepath}")
            if not dry_run:
                os.makedirs(output_filepath.parent, exist_ok=True)
                shutil.copy(input_filepath, output_filepath)
        else:
            logging.info(f"Skipping file: {input_filepath}")
