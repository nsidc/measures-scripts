import click

import run.nsidc0731.data as data


@click.group()
def cli():
    '''
    Commands for processing raw NSIDC0731 data
    '''
    pass


@cli.command()
@click.option('-i', '--input-directory',
              help='Input directory')
@click.option('-o', '--output-directory',
              help='Output directory')
@click.option('-d', '--dry-run', is_flag=True,
              help='Do a dry run')
def convert(**kwargs):
    data.convert_filepaths(**kwargs)


if __name__ == '__main__':
    cli()
