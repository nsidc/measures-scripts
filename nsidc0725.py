import click

from run.nsidc0725.convert import convert_directory, convert_shapefiles
from run.nsidc0725.magnitude import generate_magnitudes


@click.group()
def cli():
    """
    Commands for processing raw NSIDC0725 data
    """
    pass


@cli.command()
@click.option("-i", "--input-directory", help="Input directory")
@click.option("-o", "--output-directory", help="Output directory")
def convert_tif(**kwargs):
    convert_directory(**kwargs)
    convert_shapefiles(**kwargs)


@cli.command()
@click.option("-i", "--input-directory", help="Input directory")
@click.option("-o", "--output-directory", help="Output directory")
def compute_magnitudes(**kwargs):
    generate_magnitudes(**kwargs)


if __name__ == "__main__":
    cli()
